package assignmentNo_1;

import java.util.LinkedList;
import java.util.Scanner;

public class MusicList {
	
	private LinkedList<String> musicList;
    private int maxSize;

    public MusicList(int maxSize) {
        this.musicList = new LinkedList<String>();
        this.maxSize = maxSize;
    }

    public void addSong(String song) {
        if (musicList.size() >= maxSize) {
            musicList.removeFirst();
        }
        musicList.add(song);
    }

    public LinkedList<String> getMusicList() {
        return musicList;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the maximum size of the music list: ");
        int maxSize = scanner.nextInt();

        MusicList musicList = new MusicList(maxSize);

        while (true) {
            System.out.print("Enter a song (or 'quit' to exit): ");
            String song = scanner.nextLine();

            if (song.equals("quit")) {
                break;
            }

            musicList.addSong(song);
            System.out.println("Current music list: " + musicList.getMusicList());
        }

        scanner.close();
    }

}
